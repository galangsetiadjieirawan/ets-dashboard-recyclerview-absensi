package com.galangsetiadjieirawan_10191032.dashboardabsensi;

import java.util.ArrayList;

public class DatasData {
    private static String[] datasNames = {
            "Alvian",
            "Arifin",
            "Dede",
            "Fanny",
            "Febrian",
            "Galang",
            "Nabila",
            "Yasmin",
    };
    private static String[] datasDetails = {
            "Jam : 07.20 WITA",
            "Jam : 08.00 WITA",
            "Jam : 07.25 WITA",
            "Jam : 06.00 WITA",
            "Jam : 09.20 WITA",
            "Jam : 10.50 WITA",
            "Jam : 08.30 WITA",
            "Jam : 07.25 WITA"
    };
    private static int[] datasImages = {
            R.drawable.usr_alvian,
            R.drawable.usr_arifin,
            R.drawable.usr_dede,
            R.drawable.usr_fanny,
            R.drawable.usr_febrian,
            R.drawable.usr_galang,
            R.drawable.usr_nabila,
            R.drawable.usr_yasmin
    };
    static ArrayList<Data> getListData() {
        ArrayList<Data> list = new ArrayList<>();
        for (int position = 0; position < datasNames.length; position++) {
            Data data = new Data();
            data.setName(datasNames[position]);
            data.setDetail(datasDetails[position]);
            data.setPhoto(datasImages[position]);
            list.add(data);
        }
        return list;
    }
}
